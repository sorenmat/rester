package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"text/tabwriter"

	"github.com/fatih/color"

	"gopkg.in/yaml.v2"
)

type EndPointCall struct {
	Name         string `yaml:"name,omitempty`
	Operation    string `yaml:"operation,omitempty`
	Uri          string `yaml:"uri,omitempty`
	Accept       string `yaml:"accept,omitempty`
	Expected     int    `yaml:"expected,omitempty`
	ExpectedData string `yaml:"expecteddata,omitempty`
	ReturnType   string `yaml:"returntype,omitempty`
}

func fromYaml(data []byte) []EndPointCall {
	call := []EndPointCall{}

	err := yaml.Unmarshal(data, &call)
	if err != nil {
		log.Fatalf("yaml unmarshaling error: %v", err)
	}
	return call
}

func error(name string, msg string) {
	w := new(tabwriter.Writer)

	w.Init(os.Stdout, 20, 8, 2, '\t', 0)
	red := color.New(color.FgRed).SprintFunc()
	fmt.Fprintf(w, "[%v] %v: %v\n", red("𐄂"), name, msg)
	w.Flush()
}
func sucess(name string) {
	w := new(tabwriter.Writer)

	w.Init(os.Stdout, 20, 8, 2, '\t', 0)
	green := color.New(color.FgGreen).SprintFunc()
	fmt.Fprintf(w, "[%v] %v\n", green("✓"), name)

	w.Flush()
}

func executeCall(c EndPointCall) {
	switch c.Operation {
	case "GET":
		resp, err := http.Get(c.Uri)
		if err != nil {
			error(c.Name, "unable to call uri")
			return
		}
		if c.Expected != resp.StatusCode {
			error(c.Name, fmt.Sprintf("expected status code %v but it was %v", c.Expected, resp.StatusCode))
		}
		if resp == nil {
			log.Println("check if resp matches case:", resp)
		}
		if c.ExpectedData != "" { // TODO check for json
			data, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				error(c.Name, "Unable to get body data")
				return
			}
			if c.ReturnType == "application/json" {
				isJsonEqual(c.Name, []byte(c.ExpectedData), data)
			}

		}
		sucess(c.Name)

	case "POST":
		log.Println("POST")

	default:
		panic("unrecognized escape character")
	}
}

func isJsonEqual(name string, expectedData, actualData []byte) {
	buffer := new(bytes.Buffer)
	if err := json.Compact(buffer, actualData); err != nil {
		fmt.Println(err)
	}

	expectedbuffer := new(bytes.Buffer)
	if err := json.Compact(expectedbuffer, expectedData); err != nil {
		fmt.Println(err)
	}
	if buffer.String() != expectedbuffer.String() {
		error(name, fmt.Sprintf("expected data '%v' but it was '%v'", string(expectedData), string(actualData)))
	}

}

func main() {
	//	k := []EndPointCall{EndPointCall{Name: "Testing"}, EndPointCall{Name: "Testing2"}}
	//	d, _ := yaml.Marshal(&k)
	//	fmt.Println(string(d))
	files, _ := ioutil.ReadDir("specs")
	for _, f := range files {
		fmt.Println(f.Name())
		data, err := ioutil.ReadFile("specs/" + f.Name())
		if err != nil {
			log.Fatalf("yaml read error: %v", err)
		}

		call := fromYaml(data)
		for _, c := range call {
			executeCall(c)
		}

	}
}
